## RESTful JSON API Specifications for the gcode server running on the CNC controller

``/login``
* Request POST
```JSON
{
  "username":"Name",
  "password":"SecretPw1234"
}
```
* Response success
```JSON
{
  "success":true,
  "token":"uuid4tokenthingy"
}
```
* Response failure
```JSON
{
  "success":false
}
```

``/logout``
* Request POST
```JSON
{
  "token":"uuid4tokenthingy",
  "tgt":"other uuid4tokenthingy"
}
```
* Response success
```JSON
{
  "success":true
}
```
* Response failure
```JSON
{
  "success":false
}
```

``/uploadFile``
* Request POST
```JSON
{
  "token":"uuid4tokenthingy",
}
```
plus file in POST request

* Response success
```JSON
{
  "success":true,
  "filenumber":100
}
```
every file gets an ID on the server

* Response failure
```JSON
{
  "success":false
}
```

``/runFile/<int:file_id>``
* Request POST
```JSON
{
  "token":"uuid4tokenthingy"
}
```
* Response success
```JSON
{
  "success":true,
  "jobToken":"uuid4tokenthingy"
}
```
* Response failure
```JSON
{
  "success":false
}
```

``/runLine``
* Request POST
```JSON
{
  "token":"uuid4tokenthingy",
  "line":"Some gcodes"
}
```

* Response success
```JSON
{
  "success":true,
  "jobToken":"uuid4tokenthingy"
}
```
* Response failure
```JSON
{
  "success":false
}
```

``/status/<str:jobToken>``
* Request POST
```JSON
{
  "token":"uuid4tokenthingy"
}
```
* Response success
```JSON
{
  "success":true,
  "running":true
}
```
"running" can be false too

* Response failure
```JSON
{
  "success":false
}
```
