import time
import serial


class RunConfig:
    s = serial.Serial("/dev/ttyS0", 115200)
    code_queue = []

    def add_code(self, code):
        print("Adding code: "+code)
        self.code_queue.append(code)

    def run_this(self):
        while 1:
            if len(self.code_queue) != 0:
                code = self.code_queue.pop()
                print("poped code: "+code)
                self.send(code)

    def send(self, code):
        print("sending wakeup")
        self.s.write("\r\n\r\n")
        print("sent, sleeping ...")
        time.sleep(2)
        print("flushing input")
        self.s.flushInput()
        print("input flushed sending code")
        for line in code.splitlines():
            line = line.strip()
            self.s.write(line + "\n")
            print("line sent")
            grbl_out = self.s.readline().strip()
            print(grbl_out)
