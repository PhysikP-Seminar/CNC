## CNC print machine implementation

For api specs take a look at API-SPECS.md

The app consists of a RESTful JSON based API running on the CNC controller. And the electron based client which connects to the API to register and execute gcodes as jobs on the CNC controller.
