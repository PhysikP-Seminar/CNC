from app import create_app
from config import RunConfig


app = create_app()
if __name__ == "__main__":
    print("Startin")
    app.run(host="0.0.0.0", port="80")
