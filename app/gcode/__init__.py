from flask import Blueprint

bp = Blueprint('gcode', __name__)
from app.gcode import routes
