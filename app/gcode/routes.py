from app.gcode import bp
from app import app
from flask import jsonify, request


@bp.route("/run_code/", methods=["POST"])
def runFile():
    code = request.json
    code = code["code"]
    app.config["gcode_serial"].add_code(code)
    return jsonify({"success": True, "status": "Code has been added to queue"})
