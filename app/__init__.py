import flask
from config import RunConfig
import threading

app = flask.Flask(__name__)

def create_app():
    config = RunConfig()
    app.config["gcode_serial"] = config
    t = threading.Thread(target=config.run_this)
    t.start()

    from app.gcode import bp as gcode_bp
    app.register_blueprint(gcode_bp)

    @app.route("/")
    def index_method():
        return "Hello this is the api server of the P Seminar CNC Projekt"

    return app
