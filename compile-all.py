import py_compile
import os

# maybe wrong name for the file
# for checking if the python project would actually be runnable, before starting unittests in order to save time

for path, dirs, files in os.walk(os.path.abspath(os.curdir)):
    for filename in files:
        if filename.endswith(".py"):
            filename = os.path.join(path, filename)
            print(filename)
            py_compile.compile(filename)
